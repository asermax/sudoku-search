package sudokusearch.search;

import java.util.Collection;
import sudokusearch.sudoku.JSudokuTable;

/**
 * 
 * @author kiira
 * @version 1.0, Oct 23, 2015
 */
public abstract class SearchState {
    public SearchState(JSudokuTable current) {
        this.initState();
        this.pushState(current);
    }

    public JSudokuTable getCurrent() {
        return this.peekState();
    }

    public boolean isSolved() {
        return this.getCurrent().isComplete();
    }

    public JSudokuTable next() {
        return this.popState();
    }

    public void pushStates(Collection<JSudokuTable> states) {
        for (JSudokuTable state : states) {
            this.pushState(state);
        }
    }

    public abstract void initState();
    public abstract boolean hasNext();
    protected abstract void pushState(JSudokuTable state);
    public abstract JSudokuTable peekState();
    protected abstract JSudokuTable popState();
}
