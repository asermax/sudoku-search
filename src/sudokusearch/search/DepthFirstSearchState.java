package sudokusearch.search;

import java.util.LinkedList;
import java.util.List;
import sudokusearch.sudoku.JSudokuTable;

/**
 * 
 * @author kiira
 * @version 1.0, Oct 23, 2015
 */
public class DepthFirstSearchState extends SearchState {
    List<JSudokuTable> stack;

    public DepthFirstSearchState(JSudokuTable current) {
        super(current);
    }

    @Override
    public void initState() {
        this.stack = new LinkedList<>();
    }

    @Override
    public boolean hasNext() {
        return !this.stack.isEmpty();
    }

    @Override
    protected void pushState(JSudokuTable state) {
        this.stack.add(0, state);
    }

    @Override
    public JSudokuTable peekState() {
        return this.stack.get(0);
    }

    @Override
    protected JSudokuTable popState() {
        return this.stack.remove(0);
    }
}