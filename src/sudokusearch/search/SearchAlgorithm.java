package sudokusearch.search;

import sudokusearch.sudoku.JSudokuTable;

/**
 * 
 * @author kiira
 * @version 1.0, Oct 23, 2015
 */
public class SearchAlgorithm {
    private final SearchState state;

    public SearchAlgorithm(SearchState state) {
        this.state = state;
    }

    public void generateChildStates() {
        JSudokuTable currentTable = this.state.next();
        JSudokuTable nextTable;

        int[] rowCol = new int[2];
        int[] mask = new int[10];
        currentTable.getNextFreeField(rowCol, mask);

        for (int i = 1; i < mask.length; i++) {
            if (mask[i] != 0) {
                nextTable = currentTable.clone();
                nextTable.setCellValue(rowCol[0], rowCol[1], i);
                state.pushState(nextTable);
            }
        }
    }
}
