package sudokusearch.search;

import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;
import sudokusearch.sudoku.JSudokuTable;

/**
 * 
 * @author kiira
 * @version 1.0, Oct 25, 2015
 */
public class AStarSearchState extends SearchState {
    private Queue<SudokuTableWrapper> queue;

    public AStarSearchState(JSudokuTable current) {
        super(current);
    }

    public int getCurrentDepth() {
        int depth = 0;
        SudokuTableWrapper wrapper = this.queue.peek();

        if (wrapper != null) {
            depth = wrapper.getDepth();
        }

        return depth;
    }

    @Override
    public void initState() {
        this.queue = new PriorityQueue<>();
    }

    @Override
    public boolean hasNext() {
        return !this.queue.isEmpty();
    }

    @Override
    protected void pushState(JSudokuTable state) {
        this.queue.offer(new SudokuTableWrapper(state, this.getCurrentDepth() + 1));
    }

    @Override
    public JSudokuTable peekState() {
        return this.queue.peek().getTable();
    }

    @Override
    protected JSudokuTable popState() {
        return this.queue.poll().getTable();
    }

    private class SudokuTableWrapper implements Comparable<SudokuTableWrapper> {
        private final JSudokuTable table;
        private final int score;
        private final int depth;

        public SudokuTableWrapper(JSudokuTable table, int depth) {
            this.table = table;
            this.depth = depth;

            Boolean[] fullRow = new Boolean[]{
                true, true, true, true, true, true, true, true, true
            };
            Boolean[] fullCol = new Boolean[]{
                true, true, true, true, true, true, true, true, true
            };
            Boolean[] fullSquare = new Boolean[]{
                true, true, true, true, true, true, true, true, true
            };
            Boolean[] fullMiniRow = new Boolean[]{
                true, true, true, true, true, true, true, true, true,
                true, true, true, true, true, true, true, true, true,
                true, true, true, true, true, true, true, true, true
            };
            Boolean[] fullMiniCol = new Boolean[]{
                true, true, true, true, true, true, true, true, true,
                true, true, true, true, true, true, true, true, true,
                true, true, true, true, true, true, true, true, true
            };
            boolean free;
            int squareIndex, miniRowIndex, miniColIndex;

            for (int row = 0; row < 9; row++) {
                for (int col = 0; col < 9; col++) {
                    free = table.getCellValue(row, col) == 0;
                    fullRow[row] = fullRow[row] && !free;
                    fullCol[col] = fullCol[col] && !free;

                    squareIndex = row / 3 * 3 + col / 3;
                    fullSquare[squareIndex] = fullSquare[squareIndex] && !free;

                    miniRowIndex = row * 3 + col / 3;
                    fullMiniRow[miniRowIndex] = fullMiniRow[miniRowIndex] && !free;

                    miniColIndex = row / 3 * 9 + col;
                    fullMiniCol[miniColIndex] = fullMiniCol[miniColIndex] && !free;
                }
            }

            this.score = Collections.frequency(Arrays.asList(fullRow), true) * 500 +
                         Collections.frequency(Arrays.asList(fullCol), true) * 500 +
                         Collections.frequency(Arrays.asList(fullSquare), true) * 200 +
                         Collections.frequency(Arrays.asList(fullMiniRow), true) * 10 +
                         Collections.frequency(Arrays.asList(fullMiniCol), true) * 10 - 
                         depth * 5;
        }

        public JSudokuTable getTable() {
            return table;
        }

        public int getDepth() {
            return depth;
        }
        
        @Override
        public int compareTo(SudokuTableWrapper o) {
            return o.score - this.score;
        }
    }
}
