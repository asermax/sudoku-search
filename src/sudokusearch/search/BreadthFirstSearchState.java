package sudokusearch.search;

import java.util.LinkedList;
import java.util.Queue;
import sudokusearch.sudoku.JSudokuTable;

/**
 * 
 * @author kiira
 * @version 1.0, Oct 23, 2015
 */
public class BreadthFirstSearchState extends SearchState {
    Queue<JSudokuTable> queue;

    public BreadthFirstSearchState(JSudokuTable current) {
        super(current);
    }

    @Override
    public void initState() {
        this.queue = new LinkedList<>();
    }

    @Override
    public boolean hasNext() {
        return !this.queue.isEmpty();
    }

    @Override
    protected void pushState(JSudokuTable state) {
        this.queue.offer(state);
    }

    @Override
    public JSudokuTable peekState() {
        return this.queue.peek();
    }

    @Override
    protected JSudokuTable popState() {
        return this.queue.poll();
    }
}