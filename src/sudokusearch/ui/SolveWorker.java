package sudokusearch.ui;

import java.util.List;
import javax.swing.SwingWorker;
import sudokusearch.search.SearchAlgorithm;
import sudokusearch.search.SearchState;
import sudokusearch.sudoku.JSudokuTable;

/**
 * 
 * @author kiira
 * @version 1.0, Oct 25, 2015
 */
public class SolveWorker extends SwingWorker<JSudokuTable, JSudokuTable> {
    private final JSudokuGrid grid;
    private final SearchState state;

    public SolveWorker(JSudokuGrid grid, SearchState state) {
        this.grid = grid;
        this.state = state;
    }

    @Override
    protected JSudokuTable doInBackground() throws Exception {
        SearchAlgorithm search = new SearchAlgorithm(this.state);

        while (this.state.hasNext() && !this.state.isSolved()) {
            search.generateChildStates();
            this.publish(this.state.getCurrent());
            Thread.sleep(50);
        }

        return this.state.getCurrent();
    }

    @Override
    protected void process(List<JSudokuTable> chunks) {
        this.grid.updateTable(chunks.get(chunks.size() - 1));
    }
}
