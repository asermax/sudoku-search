package sudokusearch.ui;

import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;
import sudokusearch.SudokuFrame;
import sudokusearch.sudoku.JSudokuTable;

/**
 * 
 * @author kiira
 * @version 1.0, Oct 25, 2015
 */
public class CreateWorker extends SwingWorker<JSudokuTable, Object> {
    private final SudokuFrame frame;
    private final JSudokuGrid[] grids;

    public CreateWorker(SudokuFrame frame, JSudokuGrid... grids) {
        this.frame = frame;
        this.grids = grids;
    }

    @Override
    protected JSudokuTable doInBackground() throws Exception {
        this.frame.enableButtons(false);
        JSudokuTable table = new JSudokuTable();
        table.create(0);
        return table;
    }

    @Override
    protected void done() {
        try {
            for (JSudokuGrid grid : this.grids) {
                grid.updateTable(this.get());
            }
        } catch (InterruptedException | ExecutionException ignore) {
        } finally {
            this.frame.enableButtons(true);
        }
    }
}
